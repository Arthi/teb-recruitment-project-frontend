export const state = () => ({
  todo: [],
  done: []
})

export const mutations = {
  load(state, {todo, done}) {
    state.todo = todo;
    state.done = done;
  },
  setAsDone(state, task) {

    task.completed = true;
    task.overlay = false;

    state.todo.splice(state.todo.indexOf(task), 1);
    state.done.push(task);

  },
  start(state, task) {

    task.overlay = true;

  }
}
