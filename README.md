# TEB Recruitment Project - Frontend
Projekt rekrutacyjny TEB, w Vue.js z NuxtJs

## Uruchomienie
Aby uruchomić projekt, należy w terminalu (np CMD) w katalogu głównym projektu wykonać polecenie:

```
npm install & npm run dev
```

NPM ponienien zainstalować wszystkie wymagane paczki po czym przystapić do kompilacji. Następnie w przeglądarce należy otworzyć stronię `http://localhost:3000/`
